import React, {useEffect} from 'react'
import {Redirect, Route, Switch} from 'react-router-dom'
import {connect} from "react-redux"

import Layout from "./hoc/Layout/Layout"
import Quiz from "./containers/Quiz/Quiz"
import Auth from "./containers/Auth/Auth"
import QuizList from "./containers/QuizList/QuizList"
import QuizCreator from "./containers/QuizCreator/QuizCreator"
import Logout from "./components/Logout/Logout"
import {autoLogin} from "./redux/actions/auth";

function App({ isAuthenticated, autoLogin }) {
    useEffect(() => {
        autoLogin();
    }, [autoLogin]);

    let routers = (
        <Switch>
            <Route path="/" exact component={QuizList}/>
            <Route path="/auth" component={Auth}/>
            <Route path="/quiz/:id" component={Quiz}/>

            <Redirect to="/" />
        </Switch>
    );

    if (isAuthenticated) routers = (
        <Switch>
            <Route path="/" exact component={QuizList}/>
            <Route path="/logout" component={Logout}/>
            <Route path="/quiz-creator" component={QuizCreator}/>
            <Route path="/quiz/:id" component={Quiz}/>

            <Redirect to="/" />
        </Switch>
    );

    return (
        <Layout>
            { routers }
        </Layout>
    );
}

const mapStateToProps = state => ({
    isAuthenticated: !!state.auth.token
});

const mapDispatchToProps = dispatch => ({
    autoLogin: () => dispatch(autoLogin()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App)

import React, {useEffect} from 'react'
import {NavLink} from "react-router-dom"
import {connect} from 'react-redux'

import s from './QuizList.module.sass'

import Loader from "../../components/UI/Loader/Loader"
import {fetchQuizzes} from "../../redux/actions/quiz"
import UniversalContainer from "../../hoc/UniversalContainer/UniversalContainer"

const QuizList = React.memo(({ loading, quizList, fetchQuizzes }) => {
    useEffect(() => {
        fetchQuizzes()
    }, [fetchQuizzes]);

    const renderQuizzes = () => quizList.map(quiz => {
        return <li key={quiz.id}>
            <NavLink to={`/quiz/${quiz.id}`} activeClassName={s.active}>{quiz.name}</NavLink>
        </li>
    });

    return (
        <UniversalContainer stylesContent={ {background: 'linear-gradient(90deg, #fd8355 0%, #f0567c 37%, #f79cbd 100%'} }>
            <h1>Quiz List</h1>

            {
                loading && !quizList.length
                    ? <Loader />
                    : <ul className={s.list}>{ renderQuizzes() }</ul>
            }
        </UniversalContainer>
    );
});

const mapStateToProps = state => ({
    quizList: state.quiz.quizzes,
    loading: state.quiz.loading
});

const mapDispatchToProps = dispatch => ({
    fetchQuizzes: () => dispatch(fetchQuizzes())
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizList)

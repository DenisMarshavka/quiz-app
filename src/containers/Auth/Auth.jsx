import React from 'react'
import Button from "../../components/UI/Button/Button"
import {connect} from "react-redux"

// import s from './Auth.module.sass'

import Input from "../../components/UI/Input/Input"
import { checkFieldIsValid, validateForm } from '../../helpers/form/formFramework'
import {auth} from "../../redux/actions/auth"
import UniversalContainer from "../../hoc/UniversalContainer/UniversalContainer"
import Form from "../../hoc/Form/Form"

const initialFormControls = () => ({
    email: {
        value: '',
        type: 'email',
        name: 'email',
        valid: false,
        label: 'Email',
        placeholder: 'Enter your Email',
        touched: false,
        errorMessage: '',
        validation: {
            email: true,
            required: true,
            minLength: 4,
        }
    },

    password: {
        value: '',
        type: 'password',
        name: 'password',
        valid: false,
        label: 'Password',
        placeholder: 'Enter your Password',
        touched: false,
        errorMessage: '',
        validation: {
            email: false,
            required: true,
            minLength: 6,
        }
    }
});

class Auth extends React.Component {
    state = {
        isFormValid: false,
        formControls: initialFormControls()
    };

    handleLogIn = () => {
        this.props.auth(this.state.formControls.email.value, this.state.formControls.password.value, true);
    };

    handleSingUp = () => {
        this.props.auth(this.state.formControls.email.value, this.state.formControls.password.value, false);
    };

    handleSubmit = e => e.preventDefault();

    handleOnChange = (e, inputName) => {
        const formControls = { ...this.state.formControls };
        const control = { ...formControls[inputName] };

        control.value = e.target.value;
        control.touched = true;
        control.valid = checkFieldIsValid(e.target.value, control.validation);

        formControls[inputName] = control;

        this.setState({ formControls, isFormValid: validateForm(formControls) })
    };

    inputsRender = () => Object.keys(this.state.formControls).map((controlName, i) => {
        const control = this.state.formControls[controlName];

        return <Input
                    key={controlName + i}
                    type={control.type}
                    value={control.value}
                    name={control.name}
                    valid={control.valid}
                    touched={control.touched}
                    placeholder={control.placeholder}
                    label={control.label}
                    errorMessage={control.errorMessage}
                    shouldValidate={!!control.validation}
                    onBlur={e => this.handleOnChange(e, controlName)}
                    onChange={e => this.handleOnChange(e, controlName)}
                />;
    });

    render() {
        return (
            <UniversalContainer>
                <h1>Authorization</h1>

                <Form onSubmit={this.handleSubmit} style={ {maxWidth: 600} } buttons={[
                    <Button key="Log In" type="success" onClick={this.handleLogIn} disabled={!this.state.isFormValid}>Log In</Button>,
                    <Button key="Sing Out" type="primary" onClick={this.handleSingUp} style={{marginRight: 0}} disabled={!this.state.isFormValid}>Sign up</Button>
                ]}>
                    { this.inputsRender() }
                </Form>
            </UniversalContainer>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    auth: (email, password, isAuth) => dispatch(auth(email, password, isAuth))
});

export default connect(null, mapDispatchToProps)(Auth)

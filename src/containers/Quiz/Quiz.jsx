import React, {PureComponent} from 'react'
import {connect} from 'react-redux'

import ActiveQuiz from "../../components/ActiveQuiz/ActiveQuiz"
import FinishedQuiz from "../../components/FinishedQuiz/FinishedQuiz"
import Loader from "../../components/UI/Loader/Loader"

import s from './Quiz.module.sass'
import { fetchQuizById, quizAnswerClick, quizRetry } from "../../redux/actions/quiz"
import UniversalContainer from "../../hoc/UniversalContainer/UniversalContainer"

class Quiz extends PureComponent {
     componentDidMount() {
        this.props.fetchQuizById(this.props.match.params.id);
    }

    componentWillUnmount() {
         this.props.onRetry()
    }

    render() {
        return (
            <UniversalContainer stylesContent={ {background: 'linear-gradient(90deg, #5041b2 0%, #7969e6 100%)'} }>
                {
                    !this.props.isFinished
                        ? <h1 style={ {fontSize: 21, marginBottom: 50} }>Please take responses to all questions</h1>
                        : <h1 style={ {fontSize: 21, marginBottom: 50} }>Finished</h1>
                }

                <div className={s.content}>
                    {
                        this.props.loading || !this.props.quiz
                            ? <Loader containerStyle={ {maxWidth: 400} } />
                            : !this.props.isFinished
                            ? <ActiveQuiz
                                answers={this.props.quiz[this.props.activeQuestion].answers}
                                question={this.props.quiz[this.props.activeQuestion].question}
                                activeQuestion={this.props.activeQuestion + 1}
                                questionsLength={this.props.quiz.length}
                                statusAnswer={this.props.answerState}
                                handleAnswerClick={this.props.quizAnswerClick}
                            />
                            : <FinishedQuiz results={this.props.results} quiz={this.props.quiz} onRetry={this.props.onRetry} />
                    }
                </div>
            </UniversalContainer>
        )
    }
}

const mapStateToProps = state => ({
    isFinished: state.quiz.isFinished,
    activeQuestion: state.quiz.activeQuestion,
    answerState: state.quiz.answerState,
    quiz: state.quiz.quiz,
    results: state.quiz.results,
    loading: state.quiz.loading,
});

const mapDispatchToProps = dispatch => ({
    fetchQuizById: id => dispatch(fetchQuizById(id)),
    quizAnswerClick: answerId => dispatch(quizAnswerClick(answerId)),
    onRetry: () => dispatch(quizRetry()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Quiz);

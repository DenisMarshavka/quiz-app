import React from 'react'
import {connect} from "react-redux"

// import s from './QuizCreator.module.sass'

import { createQuizQuestion, finishCreateQuiz } from "../../redux/actions/create"
import UniversalContainer from "../../hoc/UniversalContainer/UniversalContainer"
import Button from "../../components/UI/Button/Button"
import Input from "../../components/UI/Input/Input"
import { checkFieldIsValid, createControl, validateForm } from '../../helpers/form/formFramework'
import Auxiliary from "../../hoc/Auxiliary/Auxiliary"
import Select from "../../components/UI/Select/Select"
import Form from "../../hoc/Form/Form"

const createFormControls = () => ({
    question: createControl({
        label: 'Question',
        name: 'question',
        errorMessage: 'The question can\'t is empty',
    }, {required: true}),

    option: createOptionControl(1),
    option2: createOptionControl(2),
    option3: createOptionControl(3),
    option4: createOptionControl(4),
});

const createOptionControl = number => createControl({
    label: `Option ${number}`,
    id: number
}, {
    required: true
});


class QuizCreator extends React.Component {
    state = {
        isFormValid: false,
        rightAnswerId: 1,
        formControls: createFormControls()
    };

    handleSubmit = e => {
        e.preventDefault();
    };

    handleInputChange = (e, inputName) => {
        const formControls = { ...this.state.formControls };
        const control = { ...formControls[inputName] };

        control.value = e.target.value;
        control.touched = true;
        control.valid = checkFieldIsValid(e.target.value, control.validation);

        formControls[inputName] = control;

        this.setState({ formControls, isFormValid: validateForm(formControls) })
    };

    handleQuestionAdd = () => {
        const index = this.props.quiz.length + 1;
        const {question, option, option2, option3, option4} = this.state.formControls;

        const questionItem = {
            id: index,
            question: question.value.trim(),
            rightAnswerId: +this.state.rightAnswerId,
            answers: [
                {text: option.value.trim(), id: option.id},
                {text: option2.value.trim(), id: option2.id},
                {text: option3.value.trim(), id: option3.id},
                {text: option4.value.trim(), id: option4.id},
            ]
        };

        this.props.createQuizQuestion(questionItem);

        this.setState({
            isFormValid: false,
            rightAnswerId: 1,
            formControls: createFormControls()
        });
    };

    handleQuizCreate = () => {
        this.props.finishCreateQuiz();

        this.setState({
            isFormValid: false,
            rightAnswerId: 1,
            formControls: createFormControls()
        })
    };

    handleSelectChange = rightAnswerId => this.setState({ rightAnswerId });

    renderControls = () => Object.keys(this.state.formControls).map((controlName, i) => {
        const control = this.state.formControls[controlName];

        return (
            <Auxiliary key={controlName + i}>
                <Input
                    type={control.type}
                    value={control.value}
                    name={control.name}
                    valid={control.valid}
                    touched={control.touched}
                    placeholder={control.placeholder}
                    label={control.label}
                    errorMessage={control.errorMessage}
                    shouldValidate={!!control.validation}
                    onBlur={e => this.handleInputChange(e, controlName)}
                    onChange={e => this.handleInputChange(e, controlName)}
                />

                { i === 0 && <hr style={ {marginBottom: 25} } /> }
            </Auxiliary>
        );
    });

    render() {
        const select = <Select
                            label="Right answer"
                            value={this.state.rightAnswerId}
                            onChange={e => this.handleSelectChange(e.target.value)}
                            options={[
                                {id: 1, text: 1},
                                {id: 2, text: 2},
                                {id: 3, text: 3},
                                {id: 4, text: 4},
                            ]}
                        />;

        return (
            <UniversalContainer stylesContent={ {background: 'linear-gradient(270deg, #f0613c 0%, #f0783c 100%)'} } stylesWrapper={ {maxWidth: '40vw'} }>
                <h1 style={ {marginBottom: 50} }>Quiz Creator</h1>

                <Form onSubmit={this.handleSubmit} buttons={[
                    <Button key="Add" type="primary" style={ {width: 250} } onClick={this.handleQuestionAdd} disabled={!this.state.isFormValid}>Add new question</Button>,
                    <Button key="Create" type="success" style={ {marginRight: 0, width: 250} } onClick={this.handleQuizCreate} disabled={!this.props.quiz.length}>Create the Quiz</Button>
                ]}>
                    { this.renderControls() }

                    { select }
                </Form>
            </UniversalContainer>
        );
    }
}

const mapStateToProps = state => ({
    quiz: state.create.quiz
});

const mapDispatchToProps = dispatch => ({
    createQuizQuestion: item => dispatch(createQuizQuestion(item)),
    finishCreateQuiz: () => dispatch(finishCreateQuiz()),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizCreator)

import axios from 'axios'

export default axios.create({
    baseURL: 'https://rect-quiz-76b94.firebaseio.com/'
});

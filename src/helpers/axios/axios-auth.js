import axios from 'axios'

export const FIREBASE_AUTH_KEY = 'AIzaSyDeIw4HbE5O8UYoZCBxlSU8aedE67ErAjM';

export const createRequest = isAuth => axios.create({
    baseURL: isAuth
                ? `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${FIREBASE_AUTH_KEY}`
                : `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${FIREBASE_AUTH_KEY}`
});

export const signAPI = async ({ isAuth, email, password, returnSecureToken = true }) => await createRequest(isAuth).post('', {
    email,
    password,
    returnSecureToken
});

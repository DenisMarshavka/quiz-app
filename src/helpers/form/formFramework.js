export const createControl = (body, validation) => ({
    label: 'Field',
    name: `field-${Math.random()}`,
    valid: !validation,
    touched: false,
    errorMessage: 'The field can\'t is empty',
    value: '',
    ...body,
    validation
});

export const checkEmailValid = email => /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(email);

export const checkFieldIsValid = (value, validation) => {
    let isValid = true;

    if (!validation) return true;

    if (validation.required) isValid = value.trim() && isValid;

    if (validation.email) return checkEmailValid(value) && isValid;

    if (validation.minLength) return (value.length >= validation.minLength) && isValid;

    return isValid;
};

export const validateForm = formControls => {
    let isFormValid = true;

    // for (let control of formControls) {
    //     if (formControls.hasOwnProperty(control)) {
    //         isFormValid = formControls[control].valid && isFormValid;
    //     }
    // }

    Object.keys(formControls).forEach(field => {
        isFormValid = formControls[field].valid && isFormValid;
    });

    return isFormValid;
};


import React, {useState} from 'react'
import {connect} from "react-redux"

import MenuToggle from "../../components/Navigation/MenuToggle/MenuToggle"
import Drawer from "../../components/Navigation/Drawer/Drawer"
import Alert from "../../components/Alert/Alert"

import s from './Layout.module.css'

const Layout = ({ isAuthenticated, children }) => {
    const [isToggleOpen, setIsToggleOpen] = useState(false);

    const handleMenuClose = () => {
      setIsToggleOpen(false);
    };

    return (
        <div className={s.wrap}>
            <Alert />

            <Drawer isOpen={isToggleOpen} onClose={handleMenuClose} isAuthenticated={isAuthenticated} />
            <MenuToggle handleOnToggle={() => setIsToggleOpen(!isToggleOpen)} isOpen={isToggleOpen} />

            <main>
                {children}
            </main>
        </div>
    );
};

const mapStateToProps = state => ({
    isAuthenticated: !!state.auth.token
});

export default connect(mapStateToProps)(Layout)

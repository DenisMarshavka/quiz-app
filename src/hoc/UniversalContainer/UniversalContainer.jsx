import React from 'react'

import s from './UniversalContainer.module.sass'

const UniversalContainer = ({ stylesContent, stylesWrapper, children }) => (
    <div className={s.content} style={stylesContent}>
        <div className={s.wrapper} style={stylesWrapper}>
            { children }
        </div>
    </div>
);

export default UniversalContainer

import React from 'react'

import s from './Form.module.sass'

const Form = ({ refLink, onSubmit, style, children, buttons }) => (
    <form ref={refLink} className={s.form} onSubmit={onSubmit} style={style}>
        { children }

        <div className={s.btns}>
            { buttons }
        </div>
    </form>
);

export default Form

import axios from "../../helpers/axios/axios-quizzes"
import {
    FETCH_QUIZZES_ERROR,
    FETCH_QUIZZES_START,
    FETCH_QUIZZES_SUCCESS,
    FETCH_QUIZ_SUCCESS,
    SET_STATE_QUIZ, QUIZ_NEXT_QUESTION, FINISHED_QUIZ, QUIZ_RETRY
} from "./actionTypes"

export const fetchStart = () => ({
    type: FETCH_QUIZZES_START
});

export const fetchError = error => ({
    type: FETCH_QUIZZES_ERROR,
    error
});

//Quizzes list
export const fetchQuizzes = () => async dispatch => {
    dispatch(fetchStart());

    try {
        await axios.get('quizzes.json').then(({ data }) => {
            const quizzes = [];

            Object.keys(data).forEach((key, i) => {
                quizzes.push({
                    id: key,
                    name: `Test №${i + 1}`
                });
            });

            dispatch(fetchQuizzesSuccess(quizzes));
        });
    } catch (e) {
        dispatch(fetchError(e));
        console.error('Alert request: ' + e);
    }
};

export const fetchQuizzesSuccess = quizzes => ({
    type: FETCH_QUIZZES_SUCCESS,
    quizzes
});
//END: Quizzes list

//Quiz
export const fetchQuizById = id => async dispatch => {
    dispatch(fetchStart());

    try {
        await axios.get(`quizzes/${id}.json`)
            .then( ({ data: quiz }) => {
                dispatch(fetchQuizSuccess(quiz))
            });
    } catch (e) {
        dispatch(fetchError(e));
        console.error('Request Alert: ' + e)
    }
};

export const fetchQuizSuccess = quiz => ({
    type: FETCH_QUIZ_SUCCESS,
    quiz
});

export const setStateQuiz = (answerId, status,results) => ({
    type: SET_STATE_QUIZ,
    answerState: {[answerId]: status},
    results
});

export const quizNextQuestion = number => ({
    type: QUIZ_NEXT_QUESTION,
    number
});

export const finishedQuiz = () => ({
    type: FINISHED_QUIZ
});

export const quizRetry = () => ({
    type: QUIZ_RETRY
});

export const quizAnswerClick = answerId => (dispatch, getState) => {
    const state = getState().quiz;

    const activeQuestion = state.quiz[state.activeQuestion];
    let results = state.results;

    let isError = true;

    if (activeQuestion.rightAnswerId === answerId) {
        isError = false;

        if (state.answerState) {
            const key = Object.keys(state.answerState)[0];

            if (state.answerState[key] === 'success') return;
        }
    }

    results[state.activeQuestion + 1] = isError ? 'error' : 'success';
    dispatch(setStateQuiz(answerId, isError ? 'error' : 'success', results));

    const setTimeout = window.setTimeout(() => {
        if (isFinished()) {
            dispatch(finishedQuiz());
        } else dispatch(quizNextQuestion(state.activeQuestion + 1));

        window.clearTimeout(setTimeout);
    }, 1500);

    const isFinished = () => state.activeQuestion + 1 === state.quiz.length;
};
//END: Quiz

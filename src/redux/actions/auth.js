import { RESET_AUTH_DATA, SET_AUTH_DATA } from "./actionTypes"
import {signAPI} from "../../helpers/axios/axios-auth"
import {showAlert} from "./alert"

export const authSuccess = token => ({
    type: SET_AUTH_DATA,
    token,
});

export const autoLogout = time => dispatch => {
    setTimeout(() => {
        dispatch(logout());
    }, time * 1000);
};

export const logout = () => {
    localStorage.removeItem('idToken');
    localStorage.removeItem('localId');
    localStorage.removeItem('expirationDate');

    return {
        type: RESET_AUTH_DATA
    }
};

export const auth = (email, password, isAuth) => async dispatch => {
    try {
        const result = await signAPI({
            isAuth,

            email,
            password,
            returnSecureToken: true,
        });

        console.log(result);

        if (result) {
            const expirationDate = new Date( new Date().getTime() + (result.data.expiresIn * 1000) );

            dispatch(authSuccess(result.data.idToken));

            localStorage.setItem('idToken', result.data.idToken);
            localStorage.setItem('localId', result.data.localId);
            localStorage.setItem('expirationDate', +expirationDate);

            dispatch(autoLogout(result.data.expiresIn));

            dispatch(showAlert(!isAuth ? 'Hello! Logged successfully :)' : 'Hi! Successful login :)', 'success'));
        }
    } catch (e) {
        dispatch(showAlert('User is not found :( Please change data and try again!', 'error'));
        console.error('Request Error: ' + e);
    }
};

export const autoLogin = () => dispatch => {
    const token = localStorage.getItem('idToken');

    if (!token) {
        dispatch(logout());
    } else {
        const expirationDate = localStorage.getItem('expirationDate');

        if (expirationDate <= +new Date()) {
            dispatch(logout());
        } else {
            localStorage.setItem('idToken', token);

            dispatch(authSuccess(token));
            dispatch(autoLogout( (expirationDate - +new Date()) / 1000) );
        }
    }
};

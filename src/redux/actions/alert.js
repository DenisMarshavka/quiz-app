import {UPDATE_ALERT_STATUS} from "./actionTypes"

export const updateAlertStatus = (isActive, content, styleType) => ({
    type: UPDATE_ALERT_STATUS,
    isActive,
    content,
    styleType
});

export const showAlert = (text, type = 'warning') => dispatch => {
    dispatch(updateAlertStatus(true, text.toString(), type));

    setTimeout(() => {
        dispatch(updateAlertStatus(false, '', ''));
    }, 7000);
};

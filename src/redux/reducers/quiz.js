import {
    FETCH_QUIZZES_ERROR,
    FETCH_QUIZZES_START,
    FETCH_QUIZZES_SUCCESS,
    FETCH_QUIZ_SUCCESS,
    SET_STATE_QUIZ, QUIZ_NEXT_QUESTION, FINISHED_QUIZ, QUIZ_RETRY
} from "../actions/actionTypes"

const initialState = {
    quizzes: [],
    loading: false,
    error: null,
    isFinished: false,
    activeQuestion: 0,
    answerState: null,
    quiz: null,
    results: {},
    load: true
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_QUIZZES_START:
            return {
                ...state,
                loading: true,
            };

        case FETCH_QUIZZES_SUCCESS:
            return {
                ...state,
                loading: false,
                quizzes: action.quizzes
            };

        case FETCH_QUIZZES_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case FETCH_QUIZ_SUCCESS:
            return {
                ...state,
                quiz: action.quiz,
                loading: false
            };

        case SET_STATE_QUIZ:
            return {
                ...state,
                answerState: action.answerState,
                results: action.results
            };

        case QUIZ_NEXT_QUESTION:
            return {
                ...state,
                activeQuestion: action.number,
                answerState: null
            };

        case FINISHED_QUIZ:
            return {
                ...state,
                isFinished: true,
            };

        case QUIZ_RETRY:
            return {
                ...state,
                activeQuestion: 0,
                answerState: null,
                isFinished: false,
                results: {}
            };

        default:
            return state;
    }
};

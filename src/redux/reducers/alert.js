import {UPDATE_ALERT_STATUS} from "../actions/actionTypes"

const initialState = {
    isActive: false,
    content: '',
    type: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_ALERT_STATUS:
            return {
                ...state,
                isActive: action.isActive,
                content: action.content,
                type: action.styleType
            };

        default:
            return state;
    }
}

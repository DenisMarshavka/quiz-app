import {SET_AUTH_DATA, RESET_AUTH_DATA} from "../actions/actionTypes"

const initialState = {
    token: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_AUTH_DATA:
            return {
                ...state,
                token: action.token,
            };

        case RESET_AUTH_DATA:
            return {
                ...state,
                token: null,
            };

        default:
            return state;
    }
}

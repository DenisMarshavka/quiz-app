import React from 'react'

import AnswersList from "./AnswersList/AnswersList"
import s from './ActiveQuiz.module.sass'

const ActiveQuiz = ({ handleAnswerClick, question, answers, statusAnswer, activeQuestion, questionsLength }) => (
    <div className={s.box}>
        <div className={s.head}>
            <span>{activeQuestion}. <b>{question}</b></span>

            <small><b>{activeQuestion}</b> from {questionsLength}</small>
        </div>

        <div className={s.question}>
            <AnswersList answers={answers} handleAnswerClick={handleAnswerClick} statusAnswer={statusAnswer} />
        </div>
    </div>
);

export default ActiveQuiz

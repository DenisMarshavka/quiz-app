import React from 'react'

import s from './AnswerItem.module.sass'

const AnswerItem = ({ handleAnswerClick, answer, status }) => {
    const classes = [s.item];

    if (status) classes.push(s[status]);

    return (
        <li className={classes.join(' ')} onClick={() => handleAnswerClick(answer.id)}>
            { answer.text }
        </li>
    )
};

export default AnswerItem

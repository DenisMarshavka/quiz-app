import React from 'react'

import AnswerItem from "./AnswerItem/AnswerItem"
import s from './AnswersList.module.sass'

const AnswersList = ({ handleAnswerClick, answers, statusAnswer }) => (
    <ul className={s.list}>
        { answers.map(answer => {
            return <AnswerItem key={answer.id} handleAnswerClick={handleAnswerClick} answer={answer} status={statusAnswer ? statusAnswer[answer.id] : null} />
        }) }
    </ul>
);

export default AnswersList

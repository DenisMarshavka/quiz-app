import React from 'react'
import {connect} from "react-redux"

import s from './Alert.module.sass'

const Alert = React.memo(({ isActive, type, content }) => (
    <div className={`${s.wrapper} ${ type ? s[type] : ''} ${ isActive ? s.active : ''}`}>
        <span className={s.content}>
            <b>{ type ? type[0].toUpperCase() + type.slice(1, type.length) : '' }:</b>&nbsp;

            { content }
        </span>
    </div>
));

const mapStateToProps = state => ({
    isActive: state.alert.isActive,
    content: state.alert.content,
    type: state.alert.type,
});

export default connect(mapStateToProps)(Alert)

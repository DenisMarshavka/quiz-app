import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from "@fortawesome/fontawesome-free-solid"
import { faBars } from "@fortawesome/fontawesome-free-solid"

import s from './MenuToggle.module.sass'

const MenuToggle = ({ handleOnToggle, isOpen }) => (
    <FontAwesomeIcon icon={isOpen ? faTimes : faBars } onClick={handleOnToggle} className={`${s.btn} ${isOpen ? s.open : null}`} />
);

export default MenuToggle

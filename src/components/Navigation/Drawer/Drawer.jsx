import React from 'react'
import {NavLink} from 'react-router-dom'

import Backdrop from "../../UI/Backdrop/Backdrop"
import s from './Drawer.module.sass'

const Drawer = ({ isAuthenticated, onClose, isOpen }) => {
    const links = [
        {to: '/', label: 'List', exact: true},
    ];

    if (isAuthenticated) {
        links.push({to: '/quiz-creator', label: 'Create new Quiz', exact: false});
        links.push({to: '/logout', label: 'Log Out', exact: false});
    } else links.push({to: '/Auth', label: 'Log In', exact: false});

    const renderLinks = () => links.map((link, i) => {
        return <li key={i}>
            <NavLink to={link.to} exact={link.exact} onClick={onClose}>{link.label}</NavLink>
        </li>
    });

    console.log('Auth:', isAuthenticated);

    return (
        <>
            { isOpen && <Backdrop onClick={onClose}/> }

            <nav className={`${s.drawer} ${!isOpen ? s.close : null}`}>
                <ul>
                    { renderLinks() }
                </ul>
            </nav>
        </>
    );
};

export default Drawer

import React from 'react'
import s from './Select.module.sass'

const Select = ({ options = [], label, value, onChange, name }) => {
    const htmlFor = `${name}-${Math.random()}`;

    return (
        <div className={s.select}>
            <label htmlFor={htmlFor}>{label}</label>

            <select name={name} id={htmlFor} value={value} onChange={onChange}>
                {
                    options.map(({ id, text }, i) => {
                        return <option key={id + i} value={id.id}>{text}</option>
                    })
                }
            </select>
        </div>
    );
};

export default Select

import React from 'react'
import s from './Input.module.sass'

const Input = ({ name, value, valid, touched, errorMessage, shouldValidate, label, type = 'text', placeholder = '', onBlur, onChange }) => {
    const htmlFor = `${type}-${Math.random()}`;
    const error = errorMessage || `Please enter a valid ${label}`;

    const isInvalid = () => !valid && touched && shouldValidate;

    return (
        <div className={`${s.input} ${isInvalid() && s.invalid}`}>
            <label htmlFor={htmlFor}>{label}</label>

            <input type={type} value={value} placeholder={placeholder} id={htmlFor} name={name} onBlur={onBlur} onChange={onChange} />

            { isInvalid() && <span className={s.error}>{error}</span> }
        </div>
    );
};

export default Input

import React from 'react'
import s from './Button.module.sass'

const Button = ({ disabled, type, children, onClick, style }) => {
    return (
        <button disabled={disabled} className={`${s.btn} ${s[type]}`} onClick={onClick} style={style}>
            {children}
        </button>
    );
};

export default Button

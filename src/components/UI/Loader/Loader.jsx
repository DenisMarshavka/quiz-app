import React from 'react'
import s from './Loader.module.sass'

const Loader = ({ containerStyle, style }) => (
    <div className={s.container} style={containerStyle} title="Loading...">
        <div className={s.loader} style={style}>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
        </div>
    </div>
);

export default Loader

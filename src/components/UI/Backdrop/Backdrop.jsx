import React from 'react'
import s from './Backdrop.module.sass'

const Backdrop = ({ onClick }) => <div className={s.content} onClick={onClick}/>;

export default Backdrop

import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheck} from "@fortawesome/fontawesome-free-solid"
import {faTimes} from "@fortawesome/fontawesome-free-solid"
import {Link} from 'react-router-dom'

import Button from "../UI/Button/Button"

import s from './FinishedQuiz.module.sass'

const FinishedQuiz = React.memo(({ results, quiz, onRetry }) => {
    const successCount = Object.keys(results).reduce((total, item) => {
        if (results[item] === 'success') total++;

        return total;
    }, 0);

    return (
        <div className={s.box}>
            <ul>
                { quiz.map((questionItem, i) => {
                    const isError = results[questionItem.id] === 'error';

                    return (
                        <li key={i}>
                            <b>{i + 1}</b>.&nbsp;

                            { questionItem.question }

                            <FontAwesomeIcon icon={isError ? faTimes : faCheck} className={isError ? s.error : s.success} />
                        </li>
                    );
                }) }
            </ul>

            <p className={s.validText}>Successfully <b>{successCount}</b> from {quiz.length}</p>

            <div className={s.btns}>
                <Button onClick={onRetry} type="primary">Retry</Button>

                <Link to='/'>
                    <Button type="success" style={ {width: 300} }>Go to the list questions</Button>
                </Link>
            </div>
        </div>
    );
});

export default FinishedQuiz;
